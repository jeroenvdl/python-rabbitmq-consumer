import pika, os, logging, time
logging.basicConfig()

#sleep to give rabbitmq time to start. To do build fault tolerance.
time.sleep(15)

# Parse CLODUAMQP_URL (fallback to localhost)
url = os.environ.get('CLOUDAMQP_URL', 'amqp://guest:guest@rabbitmq/%2f')
params = pika.URLParameters(url)
params.socket_timeout = 5

while True:
    connection = pika.BlockingConnection(params) # Connect to CloudAMQP
    channel = connection.channel() # start a channel

    channel.queue_declare(queue='hello')

    channel.basic_publish(exchange='', routing_key='hello', body='Hello World!')
    print(" [x] Sent 'Hello World!'")
    connection.close()
    time.sleep(3)