This project is a example for a rabbitmq queueing system based on python. 

To start programming with this project do a `make start`. 

A extra `make build` command has also been provided to easy rebuild the containers if you changed something.

To install all the required package do a `python3 -m venv ./venv` in the root of the project.
Then install the requirements from the consumer directory.
- `source bin/activate`
- `pip3 install -r consumer/requirements.txt`
